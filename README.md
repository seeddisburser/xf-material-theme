This is a WIP hugo theme for Xalgorithms Foundation websites.

-----

Test out the theme by starting a new [Hugo](https://github.com/gohugoio/hugo) project.

```
hugo new site quickstart
```

Next, add the template as a submodule.

```
cd quickstart
git init
git submodule add https://gitlab.com/seeddisburser/xf-material-theme.git
```

Include the theme in the config.

```
echo 'theme = "xf-material-theme"' >> config.toml
```

------

Currently I have only tested the site with the following structure:


```
.
└── content
    └── foo
    |   ├── index.md  
    |   └── firstpost.md
    ├── bar
    |   ├── index.md  
    |   ├── firstpost.md
    |   └── secondpost.md  
    └── beep
        ├── index.md      
        └── firstpost.md    
```

Some features will likely break with further sub-directories.

`index.md` files are required for directories within `content`.


 
